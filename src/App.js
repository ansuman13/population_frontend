import React from 'react';
import './App.css'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'; 
import Home from './Components/Home/Home'
import Country from './Components/Country/Country'
import City from './Components/City/City'
import Population from './Components/Population/Population'
import Login from './Components/Login/login'
import Logout from './Components/Login/Logout'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isLoggedIn: false  };
  }

  render(){
    const signedInLinks = (
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="Country">Country</Link></li>
        <li><Link to="City">City</Link></li>
        <li><Link to="Population">Population</Link></li>
        <li className='auth-button'><Link to="Logout" >Logout</Link></li>
       </ul>
        )
        
        const signedOutLinks = (
          <ul>
        <li><Link to="/">Home</Link></li>
          <li className='auth-button'><Link to="Login">Login</Link></li>
        </ul> 
      )
      
      let handleAuthChange = (isAuthValue) => {
        this.setState(
            {
              isLoggedIn : isAuthValue
              }
            );
      }

      return (
        // #todo: issue is that not loggedin user may use browser api to view other url point
        <Router>
      <div className="container">

      <div className='navbar1'>
        {this.state.isLoggedIn ? signedInLinks : signedOutLinks}
        <div className='content1'>
        <Route exact path='/' component ={Home}/>
        <Route path='/Country' component ={Country}/>
        <Route path='/City' component ={City}/>
        <Route path='/Population' component ={Population}/>
        {/* <Route path='/login' component ={Login} /> */}
        {/* <Route path='/login' render={(props) => <Login {...props} isAuthed={this.state.isLoggedIn} />} */}
        <Route
          path='/login'
          render={(props) => <Login {...props} isAuthed={handleAuthChange} />}
          />
        <Route
          path='/logout'
          render={(props) => <Logout {...props} isAuthed={handleAuthChange} />}
          />
        </div>
      </div>
    </div>
   </Router>
  );
}
}

export default App;
