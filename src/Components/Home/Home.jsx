import React from 'react'
import Report from './Report'
import Search from './Search'

class Home extends React.Component {

  render(){
    return (
      <div className="container">
          <div className="jumbotron">
          <p>filter component</p>
          <Search/>
          </div>
          <Report/>
      </div>
    )
  }
}

export default Home