import React from 'react'
import axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";

class Population extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
          countryList : [],
          cityList: ['select country',],
          country:'--country--', 
          city : '--city--', 
          ageGroup: 'young',
          malePopulation:0,
          femalePopulation:0,
          companies: [],
          isloaded:false,
          data:[],
  };
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleAgeGroupChange = this.handleAgeGroupChange.bind(this);
    this.handleMalePopulationChange = this.handleMalePopulationChange.bind(this);
    this.handleFemalePopulationChange = this.handleFemalePopulationChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
    
  }
  citylist2 = []
  
  handleAgeGroupChange(event) {
    this.setState({ageGroup: event.target.value});
  }

  handleMalePopulationChange(event) {
    this.setState({malePopulation: event.target.value});
  }

  handleFemalePopulationChange(event) {
    this.setState({femalePopulation: event.target.value});
  }
  
  handleCountryChange(event) {
    let country = event.target.value
    console.log('handleCountryChange')
    // this.state.companies.filter(company => {
    //   return company.country === this.state.selectedCompany
    // })
    let company1 = this.state.companies.filter(company => {
      return company.country === event.target.value
    })
    console.log(company1)
    if(company1.length>0){
      this.setState(
        {country: event.target.value,
        cityList:company1[0].cities
        });
    }else{
      alert('no city found!')
    }
    
    this.setState(
      {country: event.target.value});
  }

  handleCityChange(event) {
    console.log(this.state.cityList)
    this.setState({city: event.target.value});
  }

  componentDidMount(){
    // axios.get('/api/country/').then(resp => {
    //   console.log('Response', resp.data)
    //   this.setState(
    //     { countryList : resp.data })
    //   }).catch(err => {
    //   console.log('Error', err.response.status)
    // });

    axios.get('/api/city/?country=abcd').then(resp => {
      console.log('Response2', resp.data)
      this.setState(
        { companies : resp.data })
      }).catch(err => {
      console.log('Error', err.response)
    });
  }

  handleSubmit(event) {
    alert('was submitted: ' + this.state.city +' '+this.state.country + ' ' +this.state.ageGroup);
    event.preventDefault();

    axios.post('/api/search/',{
      country: this.state.country,
      city : this.state.city,
      ageGroup: this.state.ageGroup,
    })
        .then(rv => {
            console.log('Login', rv.data)
            this.setState({data:rv.data,
            isloaded:true})
            
            if(rv.status===200){
                alert('added population details successfully! ')
            }
            else{
                alert('login failed, Try again')   
            }
        })
        .catch(err => {
            alert('failed, Try again')
            console.log('error', err.response)
        });
  }

  displayCountries(){
    return this.state.companies.map( country => {
      return(
            <option value={country.country.toLowerCase()} key={country.country.toString()} >{country.country.toLowerCase()}</option>
        );
    })
}

displayCities(){
  return this.state.cityList.map( city => {
    return(
          <option value={city.toLowerCase()} key={city.toString()} >{city.toLowerCase()}</option>
      );
  })
}

  render() {
    
    let company1 = this.state.companies.filter(company => {
      return company.country === this.state.country
    })

    let isloaded = this.state.isloaded
    let data = this.state.data
    return (
       <div>

      <form onSubmit={this.handleSubmit}>
         <div>
         <label>
          <select required value={this.state.country} onChange={this.handleCountryChange}>
              <option value="country">Country</option>
              {this.displayCountries()}
          </select>
        </label>
        <label>
          <select required value={this.state.city} onChange={this.handleCityChange}>
              {this.displayCities()}
          </select>
        </label>
        <label>
          
          <select required value={this.state.ageGroup} onChange={this.handleAgeGroupChange}>
              
              <option value="male">male</option>
              <option value="female">female</option>
              
          </select>
            <input  className='btn btn-success' type="submit" value="Search" />
        </label>
        </div>
      </form>
        {isloaded && <div> <ReactTable
                        data={this.state.data}
                        columns={[
                          {
                            Header: "Query Result",
                            columns: [
                              {
                                Header: "Population Type",
                                id: "p-t",
                                accessor: d => d.population_type
                              },
                              {
                                Header: "Number",
                                id: "number",
                                accessor: d => d.number
                              },
                              
                            ]
                          }
                        ]}
                        defaultPageSize={3}
                        className="-striped -highlight"
                      /></div>}
    </div>
    );
  }
}

export default Population