import React from 'react'
import axios from 'axios'
import ReactTable from "react-table";
import "react-table/react-table.css";

class Report extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
              data : [],
             isLoaded: false 
      };
    }

    componentDidMount(){
        axios.get('/api/report/').then(resp => {
          console.log('Response2', resp.data)
          this.setState(
            { data : resp.data,
            isLoaded:true })
          }).catch(err => {
          console.log('Error', err.response)
        });
      }
    
    

  render(){
        let {isLoaded,items} = this.state
        if(isLoaded){
                    return(
                        <ReactTable
                        data={this.state.data}
                        columns={[
                          {
                            Header: "Top 3 Most Populated Cities",
                            columns: [
                              {
                                Header: "Country",
                                id: "country",
                                accessor: d => d.country_name
                              },
                              {
                                Header: "city",
                                id: "city",
                                accessor: d => d.city_name
                              },
                              {
                                Header: "Population",
                                id: "male_population",
                                accessor: d => d.male_population
                              }
                            ]
                          }
                        ]}
                        defaultPageSize={3}
                        className="-striped -highlight"
                      />
        
    )
    
  }
  else{
      return(
          <div>loding</div>
      )
  }
}
}

export default Report