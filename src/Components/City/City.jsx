import React from 'react'
import axios from 'axios';

class City extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
          countryList : [],
          country: 'nepal',
          city : '',  
  };
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCountryChange(event) {
    this.setState({country: event.target.value});
  }

  handleCityChange(event) {
    this.setState({city: event.target.value});
  }

  componentDidMount(){
    axios.get('/api/country/').then(resp => {
      console.log('Response', resp.data)
      this.setState(
        { countryList : resp.data })
      }).catch(err => {
      console.log('Error', err.response.status)
    });
  }

  handleSubmit(event) {
    alert('was submitted: ' + this.state.city +' '+this.state.country);
    event.preventDefault();

    axios.post('/api/city/', {country: this.state.country, city : this.state.city })
        .then(rv => {
            console.log('Login', rv.status)
            if(rv.status===200){
                alert('added ' + this.state.city + ' successfully! ')
            }
            else{
                alert('login failed, Try again')   
            }
        })
        .catch(err => {
            alert('failed, Try again')
            console.log('error', err.response)
        });
  }

  displayCountries(){
    return this.state.countryList.map( country => {
      return(
            <option value={country.toLowerCase()} key={country.toString()} >{country.toLowerCase()}</option>
        );
    })
}
  render() {

    return (
      <form onSubmit={this.handleSubmit}>
         <div>
         <label>
          Country:
          <select required value={this.state.value} onChange={this.handleCountryChange}>
              {this.displayCountries()}
          </select>
        </label>
         </div>
        <label>
          City:
          <input type="text" value={this.state.city} onChange={this.handleCityChange} />
        </label>
        <input  className='btn btn-success' type="submit" value="Submit" />
      </form>
    );
  }
}

export default City