import React from 'react'
import axios from 'axios';

class Country extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
    axios.post('/api/country/', {name: this.state.value})
        .then(rv => {
            console.log('Login', rv.status)
            if(rv.status===200){
                alert('added ' + this.state.value + ' successfully! ')
            }
            else{
                alert('login failed, Try again')   
            }
        })
        .catch(err => {
            alert('failed, Try again')
            console.log('error', err.response)
        });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Country:
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input  className='btn btn-success' type="submit" value="Submit" />
      </form>
    );
  }
}

export default Country