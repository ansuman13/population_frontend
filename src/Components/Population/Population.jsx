import React from 'react'
import axios from 'axios';

class Population extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
          countryList : [],
          cityList: ['select country',],
          country:'--country--', 
          city : '--city--', 
          ageGroup: 'young',
          malePopulation:0,
          femalePopulation:0,
          companies: []
  };
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleAgeGroupChange = this.handleAgeGroupChange.bind(this);
    this.handleMalePopulationChange = this.handleMalePopulationChange.bind(this);
    this.handleFemalePopulationChange = this.handleFemalePopulationChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
    
  }
  citylist2 = []
  
  handleAgeGroupChange(event) {
    this.setState({ageGroup: event.target.value});
  }

  handleMalePopulationChange(event) {
    this.setState({malePopulation: event.target.value});
  }

  handleFemalePopulationChange(event) {
    this.setState({femalePopulation: event.target.value});
  }
  
  handleCountryChange(event) {
    let country = event.target.value
    console.log('handleCountryChange')
    debugger;
    // this.state.companies.filter(company => {
    //   return company.country === this.state.selectedCompany
    // })
    let company1 = this.state.companies.filter(company => {
      return company.country === event.target.value
    })
    console.log(company1)
    if(company1.length>0){
      this.setState(
        {country: event.target.value,
        cityList:company1[0].cities
        });
    }else{
      alert('no city found!')
    }
    
    this.setState(
      {country: event.target.value});
  }

  handleCityChange(event) {
    console.log(this.state.cityList)
    this.setState({city: event.target.value});
  }

  componentDidMount(){
    // axios.get('/api/country/').then(resp => {
    //   console.log('Response', resp.data)
    //   this.setState(
    //     { countryList : resp.data })
    //   }).catch(err => {
    //   console.log('Error', err.response.status)
    // });

    axios.get('/api/city/?country=abcd').then(resp => {
      console.log('Response2', resp.data)
      this.setState(
        { companies : resp.data })
      }).catch(err => {
      console.log('Error', err.response)
    });
  }

  handleSubmit(event) {
    alert('was submitted: ' + this.state.city +' '+this.state.country + ' ' +this.state.ageGroup);
    event.preventDefault();

    axios.post('/api/population/',{
      country: this.state.country,
      city : this.state.city,
      ageGroup: this.state.ageGroup,
      malePopulation:this.state.malePopulation,
      femalePopulation:this.state.femalePopulation,
    })
        .then(rv => {
            console.log('Login', rv.status)
            if(rv.status===200){
                alert('added population details successfully! ')
            }
            else{
                alert('login failed, Try again')   
            }
        })
        .catch(err => {
            alert('failed, Try again')
            console.log('error', err.response)
        });
  }

  displayCountries(){
    return this.state.companies.map( country => {
      return(
            <option value={country.country.toLowerCase()} key={country.country.toString()} >{country.country.toLowerCase()}</option>
        );
    })
}

displayCities(){
  return this.state.cityList.map( city => {
    return(
          <option value={city.toLowerCase()} key={city.toString()} >{city.toLowerCase()}</option>
      );
  })
}

  render() {
    
    let company1 = this.state.companies.filter(company => {
      return company.country === this.state.country
    })

    return (
      <form onSubmit={this.handleSubmit}>
         <div>
         <label>
          Country:
          <select required value={this.state.country} onChange={this.handleCountryChange}>
              {this.displayCountries()}
          </select>
        </label>
         </div>

        

         <div>
         <label>
          City:
          <select required value={this.state.city} onChange={this.handleCityChange}>
              {this.displayCities()}
          </select>
        </label>
         </div>
         
         <div>
         <label>
          Age Group:
          <select required value={this.state.ageGroup} onChange={this.handleAgeGroupChange}>
              <option value="young">Young</option>
              <option value="old">Old</option>
              <option value="child">Child</option>
          </select>
        </label>
         </div>
        
        <div>
        <label>
          Male Population:
          <input type="number" value={this.state.malePopulation} onChange={this.handleMalePopulationChange} />
        </label>
        </div>
        
        <div>
        <label>
          Female Population:
          <input type="number" value={this.state.femalePopulation} onChange={this.handleFemalePopulationChange} />
        </label>
        </div>
        
        <input  className='btn btn-success' type="submit" value="Submit" />
      </form>
    );
  }
}

export default Population