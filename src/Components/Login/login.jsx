import React from 'react'
import axios from 'axios';
import {Redirect} from 'react-router-dom'
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";


class Login extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          username: '',
          password:'',
          redirect:false,
       };
      this.handleUsernameChange = this.handleUsernameChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleUsernameChange(event) {
      this.setState(
          {
              username : event.target.value
            }
          );
    }

    handlePasswordChange(event) {
      this.setState(
          {
              password : event.target.value
            }
          );
    }
  
    handleAuthChange = (value) => {
        this.props.isAuthed(value);            
    }

    handleSubmit(event) {
    console.log('props.isAuthed',this.props.isAuthed)
      event.preventDefault();
      axios.post('/api/login/', { username: this.state.username, password: this.state.password})
        .then(rv => {
            console.log('Login', rv.status)
            if(rv.status===200){
                this.handleAuthChange(true)
                this.setState({
                        redirect:true
                })
            }
            else{
                alert('login failed, Try again')   
            }
        })
        .catch(err => {
            alert('login failed, Try again')
            console.log('Login error', err.response)
        });      
      
    }
  
    render() {
    if (this.state.redirect) {
        return <Redirect to='/'/>;
    }
      return (
        <form onSubmit={this.handleSubmit}>
            <div>
          <label>
            Name:
            <input type="text" name="username" value={this.state.value} onChange={this.handleUsernameChange} />
            </label>
            </div>
            <div>
                <label> Password
            <input type="password" name="password" value={this.state.value} onChange={this.handlePasswordChange} />
          </label>
            </div>
          <input type="submit" value="Submit" />
        </form>
      );
    }
  }
  
  
  

export default Login
