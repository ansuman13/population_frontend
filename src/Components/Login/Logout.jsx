import React from 'react'
import axios from 'axios';
import {Redirect} from 'react-router-dom'
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";


class Logout extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          username: '',
          password:'',
          redirect:false,
       };
      this.handleUsernameChange = this.handleUsernameChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleUsernameChange(event) {
      this.setState(
          {
              username : event.target.value
            }
          );
    }

    handlePasswordChange(event) {
      this.setState(
          {
              password : event.target.value
            }
          );
    }
  
    handleAuthChange = () => {
        this.props.isAuthed(false); 
        this.setState({
            redirect:true
        })            
    }

    handleCancel = () => {
        this.props.isAuthed(true); 
        this.setState({
            redirect:true
        })            
    }

    handleSubmit(event) {
    console.log('props.isAuthed',this.props.isAuthed)
      event.preventDefault();
      axios.post('/api/login/', { username: this.state.username, password: this.state.password})
        .then(rv => {
            console.log('Login', rv)
        })
        .catch(err => {
            console.log('Login error', err.response)
        });

      alert('A name was submitted: ' + this.state.username + ' ' + this.state.password);
      
      this.handleAuthChange(false)
      this.setState({
          redirect:true
      })
    }
  
    render() {
    if (this.state.redirect) {
        return <Redirect to='/'/>;
    }
      return (
        <form onSubmit={this.handleSubmit}>
        <div>
          <p>
            Are you Sure ?
          </p>
          <input type="button" value="Yes" onClick = {this.handleAuthChange} />
          <input type="button" value="cancel" onClick = {this.handleCancel}/>
           </div>
        </form>
      );
    }
  }
  
  
  

export default Logout
